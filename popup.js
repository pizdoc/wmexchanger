window.onload = function() {
    
    var direct = localStorage.getItem("direct");
    var currentRate = localStorage.getItem("currentRate");

    if (!direct || !currentRate) {
        document.getElementById("no-data").style.display = "block";
    } else {
        document.getElementById("direct").innerText = direct;
        document.getElementById("currentRate").innerText = currentRate;
        document.getElementById("data").style.display = "block";
    }
}