var timeout = 1;

function load() {
    
    var exchtype = localStorage.getItem("exchtype");
    if (!exchtype) return;

    var request = new XMLHttpRequest();
    request.open("GET", "https://wm.exchanger.ru/asp/XMLWMList.asp?exchtype=" + exchtype);
    request.overrideMimeType("text/xml");
    request.onload = function () {

        var badge = "";

        if (request.responseXML) {
            
            var xml = request.responseXML.documentElement;
            var rate = xml.getElementsByTagName("BankRate")[0];

            if (rate !== undefined) {
                localStorage.setItem('currentRate', rate.textContent);
                badge = String(rate.textContent.substr(0, 4));
            }
        }

        if (badge !== "") {
            chrome.browserAction.setBadgeBackgroundColor({color:[255, 0, 0, 255]});
        } else {
            chrome.browserAction.setBadgeBackgroundColor({color:[190, 190, 190, 230]});
        }

        chrome.browserAction.setBadgeText({text:badge});
    };

    request.send(null);
}

window.onload = function() {
    load();
    setInterval(load, timeout * 1000 * 60);
}