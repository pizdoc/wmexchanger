function restore() {

    var request = new XMLHttpRequest();
    request.open("GET", "https://wm.exchanger.ru/asp/XMLbestRates.asp");
    request.overrideMimeType("text/xml");
    request.onload = function() {

        if (request.responseXML) {

            var xml = request.responseXML.documentElement;
            var row = xml.getElementsByTagName("row");

            var exchtype = localStorage.getItem("exchtype"),
                select = document.getElementById("direction"),
                option,
                rowDirect,
                rowExchtype;

            for (var i = 0; i < row.length; i++) {

                rowDirect   = row[i].getAttribute("Direct");
                rowExchtype = row[i].getAttribute("exchtype");

                option = document.createElement('option');
                option.setAttribute('value', rowExchtype);
                option.appendChild(document.createTextNode(rowDirect));

                if (exchtype === rowExchtype) option.selected = true;

                select.appendChild(option);
            }
        }
    };
    
    request.send(null);
}

function save() {

    var select = document.getElementById("direction"),
        status = document.getElementById("status");

    localStorage.setItem("exchtype", select.children[select.selectedIndex].value);
    localStorage.setItem("direct", select.children[select.selectedIndex].innerText);
    
    status.innerHTML = "have been saved.";

    setTimeout(function() {
        status.innerHTML = "";
    }, 750);

    chrome.extension.getBackgroundPage().window.location.reload();
}

window.onload = function() {
    restore();
    document.getElementById("save").onclick = save;
}